function shape(px,py,r,s)
 camera(-px,-py)
 local x=0 local y=0
 local c local i=1
 local a1 local a2
 local sr=sin(r)
 local cr=cos(r)
 local rx0=x*cr+y*sr+0.5
 local ry0=y*cr-x*sr+0.5
 local x1 local y1
 local rx1 local ry1
 while i<=#s do
  c=sub(s,i,i)
  a1=s2x[sub(s,i+1,i+1)]
  if(c=="c") then
   flcol=a1 color(a1) i+=2
  else
   a2=s2x[sub(s,i+2,i+2)]
   x1=x+a1-8 y1=y+a2-8
   rx1=x1*cr+y1*sr+0.5
   ry1=y1*cr-x1*sr+0.5
   if(c=="l") then
    line(rx0,ry0,rx1,ry1)
    --line(x,y,x1,y1)
   elseif(c=="f") then
    flend=s2x[sub(s,i+3,i+3)] i+=1
    camera(0,0)
    --flood(flr(rx1+px),flr(ry1+py),flcol,flend)
   end
   x=x1 y=y1 rx0=rx1 ry0=ry1
   i+=3
  end
 end
end

function flood(x,y,c,e)
 local x,y,c,e = x,y,c,e
 if(x>=0 and x<128 and
    y>=0 and y<128) then
  local p=pget(x,y)
  if(p!=c and p!=e) then
   pset(x,y,c)
   flood(x-1,y,c,e) flood(x+1,y,c,e)
   flood(x,y-1,c,e) flood(x,y+1,c,e)
  end
 end
end

function _init()
 local hexstr="0123456789abcdefg"
 local c s2x={} x2s={}
 for i=1,17 do
  c=sub(hexstr,i,i)
  s2x[c]=i-1 x2s[i-1]=c
  debug=true
 end
 
 rot = 0
end

function _update()
 rot += .003
end

function _draw()
 cls()
 camera()
 print("cpu: "..ceil(stat(1)/2048))
 --double-thick line and color change
 shape(32,32,rot,
  "c7lggl89l00t8gc6lg8")
 --red triangle blue fill
 shape(64,64,rot,
  "c8t80lcglcgl08l08lc0lc0ccf8g8")
 --green triangle dark green fill
 shape(70,68,0.37-rot,
  "cbt80lcglcgl08l08lc0lc0c3f8gb")
 --purple hexagon central spin
 shape(20,90,0.8-rot,
  "cdt80t80lg8lcglcgl4gl4gl08l08l40l40lc0lc0lg8c2f8gd")
 --yellow hexagon central anchor
 shape(64,64,rot/3,
  "c4tggtg8lg8lcglcgl4gl4gl08l08l40l40lc0lc0lg8caf8g4")
  shape(20,2,rot/3,
  "c4tggtg8lg8lcglcgl4gl4gl08l08l40l40lc0lc0lg8caf8g4")
   shape(64,20,rot/3,
  "c4tggtg8lg8lcglcgl4gl4gl08l08l40l40lc0lc0lg8caf8g4")
    shape(64,77,rot/3,
  "c4tggtg8lg8lcglcgl4gl4gl08l08l40l40lc0lc0lg8caf8g4")

    stats()
end


function stats()
    if debug then
        rectfill(0,0,127,6,6)      
        print('cpu '..ceil((stat(1)*100))..'%',1,1,8)
        print('mem '..ceil((stat(0)/2048))..'%',52,1,8)
        print('fps '..stat(7).."/"..stat(8),92,1,8)
    end
end


