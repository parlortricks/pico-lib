--[[
FD/f = forward
BK/b = back
PD/d = pen down
PU/u = pen up
PC/p = pen color for line and fill
FL/f = fill color
GT/g = goto
HD/h = heading/angle
CI/c = circle
RC/r = rectangle
LT/l = left
RT/r = right

--red triangle blue fill
"c8t80lcglcgl08l08lc0lc0ccf8g8"

--red triangle
p08,r5a,f05,l87,f64,l5a,f64,l87,f05




]]

function stats()
    if debug then
        rectfill(0,0,127,6,6)      
        print('cpu '..ceil((stat(1)*100))..'%',1,1,8)
        print('mem '..ceil((stat(0)/2048))..'%',52,1,8)
        print('fps '..stat(7).."/"..stat(8),92,1,8)
    end
end

function t2d(t)
	return 360*t
end

function d2t(d)
	return d/360
end

function draw_vec(xo,yo,rot,shape)
    local xo,yo,rot,shape=xo,yo,rot,shape
    local cmd,arg1,lvec
    local heading=1
    local cvec={xo,yo}
    local cmds = split(shape)
    local action = {	
        ["f"] = function (x) end, -- forward
        ["b"] = function (x) end, -- back
        ["d"] = function () end, -- pen down
        ["u"] = function () end, -- pen up
        ["p"] = function (x) color(x) end, -- pen colour
        ["g"] = function () end, -- goto x,y
        ["h"] = function () end, -- heading
        ["c"] = function () end, -- circle
        ["s"] = function () end, -- rectangle/square
        ["r"] = function (x) 
            heading += d2t(x)
        end, -- right CW 0-180 degrees
        ["l"] = function (x) 
            heading -= d2t(x)
        end, -- left CCW 0-180 degrees
    }  

    camera(-xo,-yo) -- set camera origin to vec position
    pset(0,0,8) -- draw dot at origin for debug purpose
    
    for i=1,#cmds do
        cmd=sub(cmds[i],1,1)
        arg1=sub(cmds[i],2,3)
        action[cmd](tonum("0x"..arg1))
        printh(heading)
    end
    
    camera() -- reset camera
end

function _init()
    debug=true
end

function _update()
end

function _draw()
    cls()
    camera()
    shape="p08,r5a,f05,l87,f64,g,p0a,l5a,f64,l87,f05"
    draw_vec(64,64,0,shape)
    stats()
end