function stats()
    if debug then
        rectfill(0,0,127,6,6)      
        print('cpu '..ceil((stat(1)*100))..'%',1,1,8)
        print('mem '..ceil((stat(0)/2048))..'%',52,1,8)
        print('fps '..stat(7).."/"..stat(8),92,1,8)
    end
end