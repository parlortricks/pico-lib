-- Draws a sprite to the screen with an outline of the specified colour
-- @param sp: The sprite number. When drawing a range of sprites, this is the upper-left corner.
-- @param x: x co-ord for sprite
-- @param y: y co-ord for sprite
-- @param w: The width of the range, as a number of sprites. Non-integer values may be used to draw partial sprites. The default is 1.0.
-- @param h: The height of the range, as a number of sprites. Non-integer values may be used to draw partial sprites. The default is 1.0.
-- @param oc: outline colour
-- @param tc: transparent colour, defaults to zero
-- @param flip_x: If true, the image is drawn inverted left to right. The default is false.
-- @param flip_y: If true, the image is drawn inverted top to bottom. The default is false.
function ospr(sp,x,y,w,h,oc,tc,flip_x,flip_y)
    local sp,x,y,w,h,oc,tc,flip_x,flip_y=sp,x,y,w,h,oc,tc or 0,flip_x,flip_y
    -- set transparent colour
    palt(tc,true)
    -- reset palette to outline colour    
    for c=1,15 do
        pal(c,oc)
    end
    -- draw outline
    for xx=-1,1 do
        for yy=-1,1 do
            spr(sp,x+xx,y+yy,w,h,flip_x,flip_y)
        end
    end
    -- reset palette
    pal()
    -- reset transparent colour
    palt(tc,true)
    -- draw final sprite
    spr(sp,x,y,w,h,flip_x,flip_y)
    -- reset palette
    pal()
end