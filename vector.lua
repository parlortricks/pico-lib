-- requires object.lua
local vec = obj:extend{}

function vec:init(x, y)
    self.x = x or 0
    self.y = y or 0
end

function vec:__add(v)
    return vec(self.x + v.x, self.y + v.y)
end

function vec:__sub(v)
    return vec(self.x - v.x, self.y - v.y)
end

function vec:__mul(n)
	return vec(self.x * n, self.y * n)
end

function vec:elemx(v)
	return vec(self.x * v.x, self.y * v.y)
end

function vec:unpack()
	return self.x, self.y
end