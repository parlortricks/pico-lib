-- Flood fill an shaped area
-- @param x: inside the are you want to full
-- @param y: inside the are you want to full
-- @param to_c: what color to fill too
function fill(x,y,to_c)
    local on_c=pget(x,y)
    if on_c==to_c then return end
        function flood(x,y)
            if pget(x,y)==on_c then
                pset(x,y,to_c)
                if (x>0) flood(x-1,y)
                if (x<127) flood(x+1,y)
                if (y>0) flood(x,y-1)
                if (y<127) flood(x,y+1)
            end
        end
    flood(x,y)
end  