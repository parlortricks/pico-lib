hex2dec={}

function gen_hex()
 for i=1,16 do
  c=sub("0123456789abcdef",i,i)
  hex2dec[c]=i-1
 end
end


function x2d(s)
 local total=0
 for i=#s,1,-1 do
	 local c = hex2dec[sub(s,i,i)]
	 total+=c * (16 ^ (#s-i))
 end
	return total
end