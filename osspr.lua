-- Draws a sprite to the screen with an outline of the specified colour
-- @param sx: The x coordinate of the upper left corner of the rectangle in the sprite sheet.
-- @param sy: The y coordinate of the upper left corner of the rectangle in the sprite sheet.
-- @param sw: The width of the rectangle in the sprite sheet, as a number of pixels.
-- @param sh: The height of the rectangle in the sprite sheet, as a number of pixels.
-- @param dx: The x coordinate of the upper left corner of the rectangle area of the screen.
-- @param dy: The y coordinate of the upper left corner of the rectangle area of the screen.
-- @param oc: outline colour
-- @param tc: transparent colour, defaults to zero
-- @param dw: The width of the rectangle area of the screen. The default is to match the image width (sw).
-- @param dh: The height of the rectangle area of the screen. The default is to match the image height (sh).
-- @param flip_x: If true, the image is drawn inverted left to right. The default is false.
-- @param flip_y: If true, the image is drawn inverted top to bottom. The default is false.
function osspr(sx,sy,sw,sh,dx,dy,oc,tc,dw,dh,flip_x,flip_y)
    local sx,sy,sw,sh,dx,dy,oc,tc,dw,dh,flip_x,flip_y=sx,sy,sw,sh,dx,dy,oc,tc or 0,dw,dh,flip_x,flip_y
    sx-=1
    sy-=1
    -- set transparent colour
    palt(tc,true)
    -- reset palette to outline colour    
    for c=1,15 do
        pal(c,oc)
    end
    -- draw outline
    for xx=-1,1 do
        for yy=-1,1 do
            sspr(sx+xx,sy+yy,sw,sh,dx,dy,dw,dh,flip_x,flip_y)
        end
    end
    -- reset palette
    pal()
    -- reset transparent colour
    palt(tc,true)
    -- draw final sprite
    sspr(sx,sy,sw,sh,dx,dy,dw,dh,flip_x,flip_y)
    -- reset palette
    pal()	
end  